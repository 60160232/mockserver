const mongoose = require('mongoose')
const User = require('./models/User')

mongoose.connect('mongodb://localhost/mydb', { useNewUrlParser: true, useUnifiedTopology: true })

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error: '))
db.once('open', function () {
  console.log('connect')
})

// kittySchema.methods.speak = function () {
//   const greeting = this.name
//     ? 'Meow name is ' + this.name
//     : "I don't have a name"
//   console.log(greeting)
// }

// const silence = new Kitten({ name: 'Silence' })
// console.log(silence.name)

// const fluffy = new Kitten({ name: 'fluffy' })
// fluffy.speak()
// fluffy.save(function (err, cat) {
//   if (err) return console.error(err)
//   cat.speak()
// })

User.find((err, users) => {
  if (err) return console.error(err)
  console.log(users)
})
